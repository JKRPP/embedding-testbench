import networkx as nx


class AdjacencyEmbedding:
    def __init__(self, G: nx.Graph):
        a = nx.to_numpy_array(G)
        embeddingDict = dict()

        for index, node in enumerate(G.nodes()):
            embeddingDict[node] = a[index]

        self.embedding = embeddingDict
