import networkx as nx
import numpy as np
import random
from numpy.linalg import norm
import embeddings.utils.orca as orca
from gensim.models import Word2Vec


class CLWalk:
    def __init__(
        self,
        G: nx.Graph,
        numWalks=10,
        numSteps=10,
        alpha=0.5,
        dimension=8,
        window=5,
    ):
        self.G = G
        self.S = self.generateSimilarityNetwork()

        self.walks = self.generateRandomWalks(numWalks, numSteps, alpha)
        self.embeddingModel = self.skipGram(dimension, window)

        self.embedding = self.generateEmbeddingDict(self.embeddingModel.wv)

    def generateSimilarityNetwork(self) -> nx.Graph:
        G = self.G

        S = nx.Graph()
        S.add_nodes_from(G.nodes)

        nodeList = list(G.nodes())

        avgDegree = int(round(np.mean(list(G.degree()))))

        ##Generate graphlet degree vectors
        graphletVectors = orca.orbit_counts("node", 4, G)

        ##Generate standardised graphlet degree vectors

        ##Calculate mean and std. deviation value for each coordinate in the GDV
        meanValues = []
        stdDevValues = []

        for coordinateIndex in range(len(graphletVectors[0])):
            coordinateValues = []
            for graphletVector in graphletVectors:
                coordinateValues.append(graphletVector[coordinateIndex])
            meanValues.append(np.mean(coordinateValues))
            stdDevValues.append(np.std(coordinateValues))

        ##Calculate SGDV for each node according to the paper
        standardisedGraphletVectors = []
        for gdv in graphletVectors:
            sgdv = []
            for coordinateIndex in range(len(gdv)):
                stdDev = stdDevValues[coordinateIndex]
                if stdDev == 0:
                    stdDev = 1
                sgdv.append(
                    (gdv[coordinateIndex] - meanValues[coordinateIndex]) / stdDev
                )
            standardisedGraphletVectors.append(sgdv)

        ##Connect every node in the similarity vector to the k most similar other nodes
        for idx, sgdv in enumerate(standardisedGraphletVectors):
            similarityScores = []
            for idx2, sgdv2 in enumerate(standardisedGraphletVectors):
                if idx == idx2:
                    continue
                ##Calculate the cosine similarity between the sgdvs
                if norm(sgdv) == 0:
                    cosineSim = np.dot(sgdv, sgdv2)
                else:
                    cosineSim = np.dot(sgdv, sgdv2) / (norm(sgdv) * norm(sgdv))

                similarityScores.append((cosineSim, idx2))
            similarityScoresOrdered = sorted(similarityScores, reverse=True)

            ##Connect every node to the k most similar other nodes
            for i in range(avgDegree):
                S.add_edge(nodeList[idx], nodeList[similarityScoresOrdered[i][1]])

        return S

    ##Performs a random walk of numSteps steps from startNode, with every step following S with prob alpha
    def randomWalk(self, startNode, numSteps, alpha) -> list:
        G = self.G
        S = self.S
        walk = []
        lastNode = startNode

        for i in range(numSteps):
            walk.append(lastNode)
            if random.random() >= alpha:
                if len(list(G.neighbors(lastNode))) == 0:
                    return walk
                lastNode = random.choice(list(G.neighbors(lastNode)))
            else:
                lastNode = random.choice(list(S.neighbors(lastNode)))

        return walk

    ##Returns a number of random walks from each node. numWalks specifies the number of walks carried out from each node, numSteps the steps in each walk
    def generateRandomWalks(self, numWalks, numSteps, alpha=0.5):
        G = self.G
        walks = []

        for v in G.nodes:
            for i in range(numWalks):
                walks.append(self.randomWalk(v, numSteps, alpha))

        return walks

    ##Executes the training of a skip-gram model from Word2Vec
    def skipGram(self, dimension, window) -> Word2Vec:
        return Word2Vec(
            self.walks, vector_size=dimension, window=window, sg=1, workers=8
        )

    ##Translates the wordVectors into an embedding dictionarys
    def generateEmbeddingDict(self, wordVectors):
        outDict = dict()

        for n in self.G.nodes:
            outDict[n] = wordVectors[n]

        return outDict
