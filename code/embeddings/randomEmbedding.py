import networkx as nx
import random


class RandomEmbedding:
    def __init__(self, G: nx.Graph, dimension=8):
        self.G = G
        embeddingDict = dict()
        for node in G.nodes():
            randomVector = []
            for i in range(dimension):
                randomVector.append(random.random() * 2 - 1)
            embeddingDict[node] = randomVector
        self.embedding = embeddingDict
