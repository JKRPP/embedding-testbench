import csv
import os
import time
import networkx as nx
import matplotlib.pyplot as plt

plt.switch_backend("agg")
from sklearn import metrics, decomposition
from sklearn.neural_network import MLPClassifier
from embeddings.hope import HOPE
from embeddings.role2vec import Role2Vec
from embeddings.node2vec import Node2Vec
from embeddings.clwalk import CLWalk
from embeddings.rolx import RolX
from embeddings.adjEmbedding import AdjacencyEmbedding
from embeddings.randomEmbedding import RandomEmbedding
from embeddings.utils.graphGenerator import GraphGenerator
import numpy as np
import optuna


class EmbeddingTestBench:
    def __init__(
        self,
        noiseProb=0.0,
        pOut=1,
        dimension=16,
        folderSuffix="",
        trialFolder="",
        fileReporting=False,
        graphtype="barbell",
        ppGroupCount=5,
        bbLabelMethod="bells",
    ):
        ##Plot the embeddings graphically?
        plotAll = False
        plotFinal = True

        ##Select if score in plot should be MLP score or sillhouette and if MLP score should be accurary or F1
        scoreInPlot = "MLP"
        self.mlpScore = "F1"
        self.cmapString = "tab20c"
        detailedOptunaLogging = False
        self.progressBar = True

        ##Properties of the graph to be generated
        self.noiseProb = noiseProb
        self.graphType = graphtype
        self.numGraphs = 1

        ##Embeddings to be trialed
        self.embeddAdjacencyEmbedding = True
        self.generateRandomEmbedding = True
        self.embeddNode2Vec = True
        self.embeddHopeKatz = True
        self.embeddHopeCN = True
        self.embeddHopeAA = True
        self.embeddCLWalk = True
        self.embeddRole2VecGraphlet = True
        self.embeddRole2VecLogBinned = True
        self.embeddRole2VecDegree = True
        self.embeddRolX = True

        ##Number of executions if testing multiple times
        self.numTests = 5

        ##Hyperparameter tuning parameters
        self.numTrials = 30
        self.trainOnFullGraphEmbedding = True

        ##MLP Classification scoring parameters
        self.maxIterMLP = 1000

        ##Proterties of the barbell graph
        self.numBellNodes = 20
        self.numBarNodes = 10
        self.labelMethod = bbLabelMethod

        ##Properties of the caveman graph
        self.cliqueCount = 2
        self.cliqueSize = 10

        ##Properties of the random circle graph
        self.circleNum = 10
        self.circleSizeRange = (2, 10)

        ##Properties of the random star graph
        self.starNum = 30
        self.starSizeRange = (3, 3)

        ##Properties of the DD graph
        self.ddGraphNum = 51

        ##Properties of the DHFR graph
        self.dhfrGraphNum = 104

        ##Properties of the facebook graph
        self.facebookFeatures = [77, 78]

        ##Properties of the planted partition graph
        self.totalNodes = 60
        self.groupCount = ppGroupCount
        self.groupSize = int(self.totalNodes / self.groupCount)
        self.pIn = 0.3
        self.pOut = pOut

        ##Properties of the random graph
        self.randomNodeCount = 10
        self.randomNodeProbability = 0.3

        ##Properties of the random walks
        self.dimension = dimension
        self.numSteps = 40
        self.numWalks = 20
        self.window = 10

        ##Properties of Node2Vec
        self.p = 0.5
        self.q = 0.5

        ##Propertiesof Role2Vec Graphlet
        self.r2vDelta = 0.5

        ##Properties of Role2Vec log binned
        self.binSize = 2

        ##Properties of CL-Walk
        self.alpha = 0.5

        ##Properties of HOPE (Katz)
        self.beta = 0.5

        ##Properties of RolX
        self.rolxMaxRecSteps = 4
        self.rolxThreshold = 2
        self.rolxRecSteps = 4
        self.rolxDelta = 0.5

        ##End of user set parameters
        if not detailedOptunaLogging:
            optuna.logging.set_verbosity(optuna.logging.WARNING)
        if fileReporting:
            self.progressBar = False

        self.trialName = folderSuffix

        self.runFolder = trialFolder + str(int(time.time())) + "_" + folderSuffix + "/"

        self.graphSet = "training"

        embeddingNames = []
        embeddings = []
        embeddingScoresSilhouetteByRun = []
        embeddingScoresMLPByRun = []

        if self.embeddAdjacencyEmbedding:
            embeddingNames.append("Adjacency Embedding")
        if self.generateRandomEmbedding:
            embeddingNames.append("Random Vectors")
        if self.embeddNode2Vec:
            embeddingNames.append("Node2Vec")
        if self.embeddHopeKatz:
            embeddingNames.append("HOPE (Katz)")
        if self.embeddHopeCN:
            embeddingNames.append("HOPE (Common Neighbor)")
        if self.embeddHopeAA:
            embeddingNames.append("Hope (Adamic-Adar)")
        if self.embeddCLWalk:
            embeddingNames.append("CL Walk")
        if self.embeddRole2VecGraphlet:
            embeddingNames.append("Role2Vec (graphlet)")
        if self.embeddRole2VecLogBinned:
            embeddingNames.append("Role2Vec (log binned)")
        if self.embeddRole2VecDegree:
            embeddingNames.append("Role2Vec (degree)")
        if self.embeddRolX:
            embeddingNames.append("RolX")

        self.mlpClassifiers = []
        self.embeddingNames = embeddingNames

        self.generateNewGraph()

        print("Generated graph")
        print("Graph size: " + str(len(list(self.G.nodes()))))
        print("Training set: " + str(len(self.trainNodes)))
        print("Validation set: " + str(len(self.valNodes)))
        print("Test set: " + str(len(self.testNodes)))

        graphFilePath = trialFolder + "graph.adjlist"
        nx.write_adjlist(self.G, graphFilePath)

        ##Tunes the algorithms
        self.tuneHyperparametersCombinedSplits()

        os.mkdir(self.runFolder)

        self.graphSet = "test"

        for run in range(self.numTests):
            print("Starting run " + str(run + 1) + "/" + str(self.numTests))
            print("Dimension = " + str(self.dimension))
            runEmbeddings = []
            runScoresSilhouette = []
            runScoresMLP = []
            runMLPs = []

            for indx, embeddingName in enumerate(embeddingNames):
                embedding = self.calculateEmbedding(embeddingName)

                mlpParameters = self.mlpClassifiers[indx].get_params()

                trainValVectors = []
                trainValLabels = []
                testVectors = []

                for node in self.trainNodes:
                    trainValVectors.append(embedding[node])
                    trainValLabels.append(self.labels[node])
                for node in self.valNodes:
                    trainValVectors.append(embedding[node])
                    trainValLabels.append(self.labels[node])
                for node in self.testNodes:
                    testVectors.append(embedding[node])

                algMLP = MLPClassifier(
                    hidden_layer_sizes=mlpParameters["hidden_layer_sizes"],
                    alpha=mlpParameters["alpha"],
                    learning_rate_init=mlpParameters["learning_rate_init"],
                    max_iter=self.maxIterMLP,
                )

                algMLP.fit(trainValVectors, trainValLabels)

                if self.mlpScore == "F1":
                    mlpPred = algMLP.predict(testVectors)
                    mlpScore = metrics.f1_score(
                        self.testLabels, mlpPred, average="macro"
                    )
                else:
                    mlpScore = algMLP.score(testVectors, self.testLabels)

                runScoresMLP.append(mlpScore)

                silhouetteScore = self.calculateSilhouetteScore(embedding)

                runEmbeddings.append(embedding)
                runScoresSilhouette.append(silhouetteScore)
                runMLPs.append(algMLP)

                print(
                    self.trialName
                    + ": Finished "
                    + embeddingName
                    + ". Score: "
                    + str(mlpScore)
                )

            embeddings.append(runEmbeddings)
            embeddingScoresSilhouetteByRun.append(runScoresSilhouette)
            embeddingScoresMLPByRun.append(runScoresMLP)

            self.makeEmbeddingsCSV(
                ("run_" + str(run + 1)), runEmbeddings, embeddingNames, runMLPs
            )

            if plotAll:
                plotTitles = []
                for i, embeddingName in enumerate(embeddingNames):
                    plotTitles.append(embeddingName + " score: " + str(runScoresMLP[i]))
                self.drawEmbeddings(runEmbeddings, plotTitles, runMLPs)
                fileName = self.runFolder + "run" + str(run + 1)
                plt.savefig(fileName, dpi=100)
                plt.close()

        self.embeddingSilhouetteScoresByEmbedding = []
        self.embeddingMLPScoresByEmbedding = []

        for index, embeddingName in enumerate(embeddingNames):
            scores = []
            mlpScores = []
            for runScores in embeddingScoresSilhouetteByRun:
                scores.append(runScores[index])
            for runScores in embeddingScoresMLPByRun:
                mlpScores.append(runScores[index])
            self.embeddingSilhouetteScoresByEmbedding.append(scores)
            self.embeddingMLPScoresByEmbedding.append(mlpScores)

        self.createInfoFile()

        if plotFinal:
            embeddingTitles = []
            for index, embeddingName in enumerate(embeddingNames):
                if scoreInPlot == "silhouette":
                    titleString = (
                        embeddingName
                        + " Score (Silhouette): "
                        + str(
                            self.embeddingSilhouetteScoresByEmbedding[index][
                                self.numTests - 1
                            ]
                        )
                        + "\n Average: "
                        + str(np.mean(self.embeddingSilhouetteScoresByEmbedding[index]))
                    )
                if scoreInPlot == "MLP":
                    titleString = (
                        embeddingName
                        + " Score (MLP): "
                        + str(
                            self.embeddingMLPScoresByEmbedding[index][self.numTests - 1]
                        )
                        + "\n Average: "
                        + str(np.mean(self.embeddingMLPScoresByEmbedding[index]))
                    )
                embeddingTitles.append(titleString)

            self.drawEmbeddings(embeddings[self.numTests - 1], embeddingTitles, runMLPs)
            if plotFinal:
                fileName = self.runFolder + "total_avg.png"
                plt.savefig(fileName, dpi=100)
                # plt.show()
                plt.close()

    def calculateEmbedding(self, embeddingName: str, graph=None):
        if not embeddingName in [
            "Node2Vec",
            "Role2Vec (graphlet)",
            "CL Walk",
            "Role2Vec (degree)",
            "Role2Vec (log binned)",
            "HOPE (Katz)",
            "HOPE (Common Neighbor)",
            "Hope (Adamic-Adar)",
            "RolX",
            "Adjacency Embedding",
            "Random Vectors",
        ]:
            raise Exception("Unknown embedding: " + embeddingName)

        if graph == None:
            graph = self.G

        if embeddingName == "Node2Vec":
            embedding = Node2Vec(
                graph,
                numWalks=self.numWalks,
                numSteps=self.numSteps,
                dimension=self.dimension,
                p=self.p,
                q=self.q,
                window=self.window,
            )
        if embeddingName == "Role2Vec (graphlet)":
            embedding = Role2Vec(
                graph,
                numWalks=self.numWalks,
                numSteps=self.numSteps,
                dimension=self.dimension,
                roles="graphlet",
                window=self.window,
                delta=self.r2vDelta,
            )

        if embeddingName == "CL Walk":
            embedding = CLWalk(
                graph,
                numWalks=self.numWalks,
                numSteps=self.numSteps,
                dimension=self.dimension,
                alpha=self.alpha,
                window=self.window,
            )

        if embeddingName == "Role2Vec (degree)":
            embedding = Role2Vec(
                graph,
                numWalks=self.numWalks,
                numSteps=self.numSteps,
                dimension=self.dimension,
                roles="degree",
                binning="log",
                window=self.window,
            )

        if embeddingName == "Role2Vec (log binned)":
            embedding = Role2Vec(
                graph,
                numWalks=self.numWalks,
                numSteps=self.numSteps,
                dimension=self.dimension,
                roles="graphlet",
                binning="log",
                window=self.window,
                binSize=self.binSize,
            )

        if embeddingName == "HOPE (Katz)":
            embedding = HOPE(
                graph, proxMeassure="katz", beta=self.beta, dimension=self.dimension
            )

        if embeddingName == "HOPE (Common Neighbor)":
            embedding = HOPE(
                graph, proxMeassure="CommonNeighbor", dimension=self.dimension
            )

        if embeddingName == "Hope (Adamic-Adar)":
            embedding = HOPE(graph, proxMeassure="AA", dimension=self.dimension)

        if embeddingName == "RolX":
            embedding = RolX(
                graph,
                dimension=self.dimension,
                threshold=self.rolxThreshold,
                recursionSteps=self.rolxRecSteps,
                delta=self.rolxDelta,
            )

        if embeddingName == "Adjacency Embedding":
            embedding = AdjacencyEmbedding(graph)

        if embeddingName == "Random Vectors":
            embedding = RandomEmbedding(graph, self.dimension)

        return embedding.embedding

    ##Call this method to generate a new graph of the type specified
    def generateNewGraph(self):
        generator = GraphGenerator(self.graphSet)
        if not self.graphType in [
            "barbell",
            "caveman",
            "randomcircle",
            "randomstars",
            "karate",
            "DHFR",
            "DD",
            "facebook",
            "pp",
            "hollowBarbell",
            "nonConnected",
            "random",
            "Cora",
            "texas",
            "chameleon",
            "amazonphoto",
        ]:
            raise Exception("Graph type not in list")

        currGraph = nx.Graph()
        currLabels = []

        for i in range(self.numGraphs):
            if self.graphType == "barbell":
                newGraph = generator.generateBarbellGraph(
                    self.numBellNodes, self.numBarNodes, self.labelMethod
                )
            if self.graphType == "hollowBarbell":
                newGraph = generator.generateHollowBarbellGraph(
                    self.numBellNodes, self.numBarNodes, self.labelMethod
                )
            if self.graphType == "caveman":
                newGraph = generator.generateCavemanGraph(
                    self.cliqueCount, self.cliqueSize
                )
            if self.graphType == "randomcircle":
                newGraph = generator.generateRandomCircleGraph(
                    self.circleNum, self.circleSizeRange
                )
            if self.graphType == "randomstars":
                newGraph = generator.generateRandomStarsGraph(
                    self.starNum, self.starSizeRange
                )
            if self.graphType == "DHFR":
                newGraph = generator.generateDHFRGraph()
            if self.graphType == "DD":
                newGraph = generator.generateDDGraph()
            if self.graphType == "facebook":
                newGraph = generator.generateFacebookGraph()
            if self.graphType == "pp":
                newGraph = generator.generatePlantedPartitionGraph(
                    groupCount=self.groupCount,
                    groupSize=self.groupSize,
                    pIn=self.pIn,
                    pOut=self.pOut,
                )
            if self.graphType == "nonConnected":
                newGraph = generator.generateNonConnectedGraph(5)
            if self.graphType == "random":
                newGraph = generator.generateRandomGraph(
                    self.randomNodeCount, self.randomNodeProbability
                )

            if self.graphType == "karate":
                newGraph = generator.generateDGLGraph("karate")
            if self.graphType == "Cora":
                newGraph = generator.generateDGLGraph("Cora")
            if self.graphType == "texas":
                newGraph = generator.generateDGLGraph("texas")
            if self.graphType == "chameleon":
                newGraph = generator.generateDGLGraph("chameleon")
            if self.graphType == "amazonphoto":
                newGraph = generator.generateDGLGraph("amazonphoto")

            noiseTuple = generator.addNoiseToGraph(newGraph[0], self.noiseProb)

            Gnoisy = noiseTuple[0]
            self.noiseEdgeNum = noiseTuple[1]

            currGraph = nx.disjoint_union(currGraph, Gnoisy)

            currLabels.extend(newGraph[1])

        self.G = currGraph
        self.labels = currLabels

        print("Nodes: " + str(self.G.number_of_nodes()))

        self.generateGraphSplits()

    ##Divides the graph into training, validation and test splits
    def generateGraphSplits(self):
        generator = GraphGenerator("training")
        graphSets = generator.createSplits(self.G)

        self.trainNodes = graphSets[0]
        self.valNodes = graphSets[1]
        self.testNodes = graphSets[2]

        self.trainLabels = []
        self.valLabels = []
        self.testLabels = []

        for node in self.trainNodes:
            self.trainLabels.append(self.labels[node])
        for node in self.valNodes:
            self.valLabels.append(self.labels[node])
        for node in self.testNodes:
            self.testLabels.append(self.labels[node])

        self.trainValGraph = nx.subgraph(self.G, self.trainNodes + self.valNodes).copy()

    ##Calculates the sillhouette score for a given embedding
    def calculateSilhouetteScore(self, embedding: dict):
        vectorList = []
        for n in self.G.nodes:
            vectorList.append(embedding[n])

        score = metrics.silhouette_score(vectorList, self.labels)
        return score

    ##Creates two CSVs: One with all the vectors for all nodes, one with all the classifications
    def makeEmbeddingsCSV(self, filePrefix, embeddings, embeddingTitles, classifiers):
        G = self.G
        labels = self.labels

        vectorFilePath = self.runFolder + filePrefix + "_vectors.csv"
        classificationFilePath = self.runFolder + filePrefix + "_classification.csv"

        firstRow = ["Node", "Label"]
        firstRow.extend(embeddingTitles)

        vectorFile = open(vectorFilePath, "a", newline="")
        vectorWriter = csv.writer(vectorFile)
        vectorWriter.writerow(firstRow)

        classificationFile = open(classificationFilePath, "a", newline="")
        classificationWriter = csv.writer(classificationFile)
        classificationWriter.writerow(firstRow)

        for node, index in enumerate(G.nodes()):
            vectorRow = [str(node), labels[index]]
            classificationRow = [str(node), labels[index]]

            for embeddingIndx in range(len(embeddings)):
                vectorRow.append(list(embeddings[embeddingIndx][node]))
                classificationRow.append(
                    classifiers[embeddingIndx].predict(
                        [embeddings[embeddingIndx][node]]
                    )[0]
                )
            vectorWriter.writerow(vectorRow)
            classificationWriter.writerow(classificationRow)

        vectorFile.close()
        classificationFile.close()

    ##Draws a Graph without edges, with the nodes positioned according to the embedding and colored according to the labels
    def drawGraphColored(self, embedding: dict, axes, classifier: MLPClassifier = None):
        G = self.G

        labels = self.labels
        colors = self.labels

        if classifier != None:
            labelDict = dict()
            for nodeID in list(embedding.keys()):
                labelDict[nodeID] = classifier.predict([embedding[nodeID]])[0]
            labels = labelDict

        embedding = embedding

        ##If dimension is greater than two, use PCA for visualisation
        if len(embedding[0]) > 2:
            pcamachine = decomposition.PCA(n_components=2)
            pcaresult = pcamachine.fit_transform(list(embedding.values()))

            embedding = dict()
            for idx, value in enumerate(pcaresult):
                embedding[idx] = value

        nx.draw_networkx(
            G,
            node_color=colors,
            labels=labels,
            pos=embedding,
            with_labels=True,
            width=0,
            node_size=50,
            font_size=10,
            ax=axes,
            cmap=plt.get_cmap(self.cmapString),
        )

    ##Draws a grid of the original graph and the given embeddings
    def drawEmbeddings(self, embeddings, embeddingTitles, classifiers=None):
        if classifiers == None:
            classifiers = self.mlpClassifiers

        totalPlotNum = len(embeddings) + 1
        xEmbeddings = int(np.floor(np.sqrt(totalPlotNum)))
        yEmbeddings = int(np.ceil(totalPlotNum / xEmbeddings))
        fig, all_axes = plt.subplots(xEmbeddings, yEmbeddings)
        fig.set_figheight(10)
        fig.set_figwidth(15)
        ax = all_axes.flat

        labelDict = dict()

        for i in range(len(self.labels)):
            labelDict[i] = self.labels[i]

        ax[0].set_title("Original graph. Noise edges: " + str(self.noiseEdgeNum))
        nx.draw_networkx(
            self.G,
            node_size=100,
            font_size=10,
            ax=ax[0],
            with_labels=True,
            node_color=self.labels,
            labels=labelDict,
            cmap=plt.get_cmap(self.cmapString),
        )

        for i in range(len(embeddings)):
            ax[i + 1].set_title(embeddingTitles[i], fontsize=10)
            self.drawGraphColored(embeddings[i], ax[i + 1], classifiers[i])

    ##Creates an info file for the run
    def createInfoFile(self):
        fileString = ""
        fileName = self.runFolder + "info.txt"

        fileString += "Info for run: \n"
        fileString += "Embeddings trialed: \n" + str(self.embeddingNames) + "\n \n"
        fileString += "Average scores: \n"

        for index, embedding in enumerate(self.embeddingNames):
            fileString += embedding + ":\n"
            fileString += (
                "MLP: " + str(np.mean(self.embeddingMLPScoresByEmbedding[index])) + "\n"
            )
            fileString += (
                "Sillhouette: "
                + str(np.mean(self.embeddingSilhouetteScoresByEmbedding[index]))
                + "\n \n"
            )

        fileString += "Tuning parameters: \n \n"

        fileString += "Number of trials: " + str(self.numTrials) + " \n"
        fileString += "Number of Tests: " + str(self.numTests) + "\n \n"
        fileString += "Maximum MLP Iterations: " + str(self.maxIterMLP) + " \n"

        fileString += "Embedding parameters: \n \n"

        fileString += "Dimension: " + str(self.dimension) + " \n"
        fileString += "Skip-gram window: " + str(self.window) + " \n"
        fileString += "Number of walks: " + str(self.numWalks) + " \n"
        fileString += "Steps per walk: " + str(self.numSteps) + " \n \n"

        fileString += "Graph properties:" + "\n \n"
        fileString += "Graph type: " + self.graphType + "\n"

        if self.graphType == "barbell" or self.graphType == "hollowBarbell":
            fileString += "Label method: " + self.labelMethod + "\n"
            fileString += (
                "Number nodes: "
                + str(self.numBellNodes)
                + " (bell), "
                + str(self.numBarNodes)
                + " (bar) \n"
            )

        if self.graphType == "caveman":
            fileString += "Clique count: " + str(self.cliqueCount) + "\n"
            fileString += "Clique size: " + str(self.cliqueSize) + "\n"

        fileString += "Noise probability: " + str(self.noiseProb) + "\n \n"

        fileString += "Tuned Hyperparameters: \n \n"

        for index, embedding in enumerate(self.embeddingNames):
            fileString += embedding + ": \n"
            if embedding == "Node2Vec":
                fileString += "p: " + str(self.p) + "\n"
                fileString += "q: " + str(self.q) + "\n"
            if embedding == "Role2Vec (degree)":
                fileString += "(no algorithm specific hyperparameters) \n"
            if embedding == "CL Walk":
                fileString += "Alpha: " + str(self.alpha) + "\n"
            if embedding == "Role2Vec (graphlet)":
                fileString += "Delta: " + str(self.r2vDelta) + "\n"
            if embedding == "Role2Vec (log binned)":
                fileString += "Bin size: " + str(self.binSize) + "\n"
            if embedding == "HOPE (Katz)":
                fileString += "Beta: " + str(self.beta) + "\n"
            if embedding == "RolX":
                fileString += "Recursion Steps: " + str(self.rolxRecSteps) + "\n"
                fileString += "Delta: " + str(self.rolxDelta) + "\n"
                fileString += "Threshold: " + str(self.rolxThreshold) + "\n"

            fileString += "\n (MLP properties:) \n"

            params = self.mlpClassifiers[index].get_params()

            fileString += "Learning rate: " + str(params["learning_rate_init"]) + "\n"
            fileString += (
                "Hidden layer size: " + str(params["hidden_layer_sizes"]) + "\n"
            )
            fileString += "Alpha: " + str(params["alpha"]) + "\n \n"

        fileString += "Detailed results: \n \n"

        for index, embedding in enumerate(self.embeddingNames):
            fileString += embedding + ": \n"
            fileString += (
                "MLP: " + str(self.embeddingMLPScoresByEmbedding[index]) + "\n"
            )
            fileString += (
                "Silhouette: "
                + str(self.embeddingSilhouetteScoresByEmbedding[index])
                + "\n \n"
            )

        open(fileName, "w").write(fileString)

    ##Tunes the hyperparameters for a given embedding
    def objectiveCombinedFunctionSplits(self, trial: optuna.trial.Trial):
        algorithm = trial.study.study_name

        if algorithm == "Node2Vec":
            self.p = trial.suggest_float("p", 0, 1)
            self.q = trial.suggest_float("q", 0, 1)

        if algorithm == "CL Walk":
            self.alpha = trial.suggest_float("alpha", 0, 1)

        if algorithm == "Role2Vec (graphlet)":
            self.r2vDelta = trial.suggest_float("delta", 0, 1)

        if algorithm == "Role2Vec (log binned)":
            self.binSize = trial.suggest_float("binSize", 1, 10)

        if algorithm == "HOPE (Katz)":
            self.beta = trial.suggest_float("beta", 0, 1)

        if algorithm == "RolX":
            self.rolxRecSteps = trial.suggest_int(
                "rolxRecSteps", 0, self.rolxMaxRecSteps
            )
            self.rolxDelta = trial.suggest_float("rolxDelta", 0, 1)
            self.rolxThreshold = trial.suggest_int("rolxThreshold", 1, 10)

        learning_rate_init = trial.suggest_float("learningRate", 0.0001, 0.1)
        hiddenLayerSize = trial.suggest_categorical(
            "Hidden layer size", [10, 100, 1000]
        )
        mlpAlpha = trial.suggest_float("mlpAlpha", low=0.01, high=10, log=False)

        if self.trainOnFullGraphEmbedding:
            trainGraphVectors = self.calculateEmbedding(algorithm, graph=self.G)
        else:
            trainGraphVectors = self.calculateEmbedding(
                algorithm, graph=self.trainValGraph
            )

        trainVectors = []

        valVectors = []

        for node in trainGraphVectors:
            if node in self.trainNodes:
                trainVectors.append(trainGraphVectors[node])
                continue
            elif node in self.valNodes:
                valVectors.append(trainGraphVectors[node])
                continue

        mlp = MLPClassifier(
            max_iter=self.maxIterMLP,
            learning_rate_init=learning_rate_init,
            hidden_layer_sizes=(hiddenLayerSize,),
            alpha=mlpAlpha,
        )

        mlp.fit(trainVectors, self.trainLabels)

        self.trainedMLPs.append(mlp)

        if self.mlpScore == "F1":
            prediction = mlp.predict(valVectors)
            score = metrics.f1_score(self.valLabels, prediction, average="micro")
        else:
            score = mlp.score(valVectors, self.valLabels)

        return score

    ##Tunes the hyperparameters for the algorithms selected according to the hyperparameters specified
    def tuneHyperparametersCombinedSplits(self):
        for alg in self.embeddingNames:
            self.trainedMLPs = []
            print(self.trialName + ": Tuning " + alg)
            embeddingStudy = optuna.create_study(study_name=alg, direction="maximize")
            embeddingStudy.optimize(
                self.objectiveCombinedFunctionSplits,
                n_trials=self.numTrials,
                callbacks=[self.stopStudyOnPerfection],
                show_progress_bar=self.progressBar,
            )

            bestTrial = embeddingStudy.best_trial
            if alg == "Node2Vec":
                self.p = bestTrial.params["p"]
                self.q = bestTrial.params["q"]
            if alg == "CL Walk":
                self.alpha = bestTrial.params["alpha"]
            if alg == "Role2Vec (graphlet)":
                self.r2vDelta = bestTrial.params["delta"]
            if alg == "Role2Vec (degree)":
                print(
                    "No algorithm specific hyperparameters for Role2Vec(degree) available"
                )
            if alg == "Role2Vec (log binned)":
                self.binSize = bestTrial.params["binSize"]
            if alg == "HOPE (Katz)":
                self.beta = bestTrial.params["beta"]
            if alg == "HOPE (Common Neighbor)":
                print(
                    "No algorithm specific hyperparameters for HOPE (Common Neighbor) available"
                )
            if alg == "Hope (Adamic-Adar)":
                print(
                    "No algorithm specific hyperparameters for Hope (Adamic-Adar) available"
                )
            if alg == "RolX":
                self.rolxThreshold = bestTrial.params["rolxThreshold"]
                self.rolxDelta = bestTrial.params["rolxDelta"]
                self.rolxRecSteps = bestTrial.params["rolxRecSteps"]
            if alg == "Adjacency Embedding":
                print(
                    "No algorithm specific hyperparameters for Adjacency Embedding available"
                )
            if alg == "Random Vectors":
                print("Random Vectors can't be tuned")

            print(
                self.trialName
                + ": Finished tuning "
                + alg
                + " best result: "
                + str(bestTrial.value)
            )
            self.mlpClassifiers.append(self.trainedMLPs[bestTrial.number])

    ##Stop the study if the value is 1.0
    def stopStudyOnPerfection(self, study, trial):
        if study.best_value == 1.0:
            study.stop()
