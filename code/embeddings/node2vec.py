import random
import time
import networkx as nx
from gensim.models import Word2Vec


class Node2Vec:
    def __init__(
        self, G: nx.Graph, p=0.5, q=0.5, numWalks=10, numSteps=20, dimension=8, window=5
    ):
        # startTime = time.time()
        self.G = G

        self.pWeight = 1 / p
        self.qWeight = 1 / q

        self.numSteps = numSteps
        self.numWalks = numWalks

        self.walks = self.generateRandomWalks()
        self.embeddingModel = self.skipGram(dimension, window)

        self.embedding = self.generateEmbeddingDict(self.embeddingModel.wv)
        # print("Time elapsed: " + str(time.time() - startTime))

    def randomWalk(self, startNode):
        walk = [startNode]
        G = self.G

        prevNode = startNode
        startEdges = list(G.neighbors(prevNode))
        if not startEdges:
            return walk
        node = random.choice(startEdges)

        for i in range(self.numSteps - 1):
            neighbors = list(G.neighbors(node))
            weights = []

            for neighbor in neighbors:
                if neighbor == prevNode:
                    weights.append(self.pWeight)
                    continue
                if G.has_edge(prevNode, neighbor):
                    weights.append(1)
                    continue
                weights.append(self.qWeight)

            prevNode = node
            node = random.choices(neighbors, weights=weights)[0]

            walk.append(node)

        return walk

    ##Returns a number of random walks from each node.
    def generateRandomWalks(self):
        G = self.G
        walks = []

        for node in G.nodes:
            for i in range(self.numWalks):
                walks.append(self.randomWalk(node))

        return walks

    def generateEmbeddingDict(self, wordVectors):
        outDict = dict()

        for n in self.G.nodes:
            outDict[n] = wordVectors[n]

        return outDict

    ##Executes the training of a skip-gram model from Word2Vec
    def skipGram(self, dimension, window) -> Word2Vec:
        return Word2Vec(
            self.walks, vector_size=dimension, window=window, sg=1, workers=8
        )
