import networkx as nx
from embeddings.utils.refex import ReFeX
import sklearn.decomposition as decomp


class RolX:
    def __init__(
        self, G: nx.Graph, dimension=8, threshold=2, recursionSteps=3, delta=0.5
    ) -> None:
        self.G = G

        featureMatrix = ReFeX(
            G, threshold=threshold, recursionSteps=recursionSteps, delta=delta
        ).featureVectors

        try:
            self.embeddingMatrix, H, n_iter = decomp.non_negative_factorization(
                X=featureMatrix, n_components=dimension, solver="mu", max_iter=1000
            )

            self.embedding = self.generateEmbeddingDict()
        except:
            self.embedding = self.generateEmptyEmbeddingOnFail(dimension)

    def generateEmbeddingDict(self):
        em = self.embeddingMatrix
        embeddDict = dict()
        for index, v in enumerate(em):
            embeddDict[index] = v

        return embeddDict

    def generateEmptyEmbeddingOnFail(self, dimension):
        emptyVector = []
        for i in range(dimension):
            emptyVector.append(0)

        embeddDict = dict()
        for n in range(self.G.number_of_nodes()):
            embeddDict[n] = emptyVector

        return embeddDict
