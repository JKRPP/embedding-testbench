import networkx as nx
import numpy as np
import scipy.sparse.linalg as la


class HOPE:
    def __init__(self, G: nx.Graph, proxMeassure, beta=0.5, dimension=8):
        ##Create adjacency and identity matrix
        a = nx.to_numpy_array(G)
        i = np.eye(G.number_of_nodes())

        ##Create mg and ml matrices for factorisation
        if proxMeassure == "katz":
            mg = i - beta * a
            ml = beta * a

        if proxMeassure == "CommonNeighbor":
            mg = i
            ml = np.dot(a, a)

        if proxMeassure == "AA":
            mg = i
            aSummation = a.sum(axis=1) + a.sum(axis=0)
            recip = []
            for num in aSummation:
                if num == 0:
                    recip.append(float(0))
                    continue
                recip.append(1 / num)
            d = np.diag(recip)
            ml = np.dot(np.dot(a, d), a)

        ##Factorise the matrices into two matrices of size n*(d/2) and (d/2)*n

        matrixSize = dimension // 2

        s = np.dot(np.linalg.inv(mg), ml)

        try:
            u, s, vh = la.svds(s, matrixSize)

            m1 = np.dot(u, np.diag(np.sqrt(s)))
            m2 = np.dot(vh.T, np.diag(np.sqrt(s)))

            ##Generate the embedding as the concatonation of matrices m1 and m2

            self.embeddingMatrix = np.concatenate((m1, m2), axis=1)

            ##Translate embedding matrix into dictionary

            self.embedding = self.generateEmbeddingDict()
        except:
            self.embedding = self.generateEmptyEmbeddingOnFail(G, dimension)

    def generateEmbeddingDict(self):
        em = self.embeddingMatrix
        embeddDict = dict()
        for index, v in enumerate(em):
            embeddDict[index] = v

        return embeddDict

    def generateEmptyEmbeddingOnFail(self, G, dimension):
        emptyVector = []
        for i in range(dimension):
            emptyVector.append(0)

        embeddDict = dict()
        for n in range(G.number_of_nodes()):
            embeddDict[n] = emptyVector

        return embeddDict
