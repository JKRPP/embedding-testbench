from math import ceil
import math
import random
import networkx as nx
import embeddings.utils.orca as orca
from gensim.models import Word2Vec


class Role2Vec:
    def __init__(
        self,
        G: nx.Graph,
        numWalks=10,
        numSteps=10,
        dimension=8,
        window=5,
        roles="graphlet",
        binning="paper",
        delta=0.5,
        binSize=2,
    ):
        self.G = G
        self.delta = delta
        self.binSize = binSize
        if not roles in ["graphlet", "degree"]:
            raise Exception("role assignment method not in list")
        if not binning in ["log", "paper"]:
            raise Exception("binning method not in list")
        if roles == "graphlet":
            self.roleDict = self.calculateRolesGraphlet(binning=binning)
        elif roles == "degree":
            self.roleDict = self.calculateRolesDegree()
        self.walks = self.generateRandomWalks(numWalks, numSteps)
        self.roleEmbedding = self.skipGram(dimension=dimension, window=window)
        self.embedding = self.nodeEmbedding()

    ##Calculate Roles based upon graphlet count
    def calculateRolesGraphlet(self, binning: str) -> dict:
        G = self.G

        ##Generate graphlet count vectors
        graphletVectors = orca.orbit_counts("node", 4, G)

        ##log-bin the counts
        if binning == "paper":
            graphletVectorsBinned = self.binVectorsPaper(graphletVectors)
        elif binning == "log":
            graphletVectorsBinned = self.logBinVectors(graphletVectors)

        ##Assign roles to nodes of the same binned graphlet count vector
        roles = dict()
        roleDict = dict()
        nodeList = list(G.nodes())
        roleIndex = 0

        for indx, binnedVector in enumerate(graphletVectorsBinned):
            vectorString = str(binnedVector)
            if not vectorString in roles:
                roles[vectorString] = roleIndex
                roleIndex += 1

            roleDict[nodeList[indx]] = roles[vectorString]

        return roleDict

    ##Bin vectors according to the method described in the paper
    def binVectorsPaper(self, graphletVectors):
        delta = self.delta
        sameValuesSameBin = True

        numVectors = len(graphletVectors)
        numFeatures = len(graphletVectors[0])

        ##calculate the log binning bin borders
        binBorders = []
        border = 0
        remainingLen = numVectors

        while remainingLen > 0:
            binsize = ceil(remainingLen * delta)
            border += binsize
            remainingLen -= binsize
            binBorders.append(border)

        ##log-bin the counts
        featureBins = []

        for featureIndex in range(numFeatures):
            featureValues = []
            featureValueBorders = []
            for featureVector in graphletVectors:
                featureValues.append(featureVector[featureIndex])

            featureValuesSorted = sorted(featureValues)
            for border in binBorders:
                featureValueBorders.append(featureValuesSorted[border - 1])
            featureBins.append(featureValueBorders)

        graphletVectorsBinned = []

        for featureVector in graphletVectors:
            binnedVector = []
            for i in range(numFeatures):
                for index, bin in enumerate(featureBins[i]):
                    if featureVector[i] <= bin:
                        binnedVector.append(index)
                        break
                    elif index == len(featureBins[i]) - 1:
                        binnedVector.append(index)
            graphletVectorsBinned.append(binnedVector)

        return graphletVectorsBinned

    ##Bin vectors according to conventional log binning
    def logBinVectors(self, graphletVectors):
        graphletVectorsBinned = []

        for vector in graphletVectors:
            binnedVector = []
            for feature in vector:
                if not feature == 0:
                    binnedVector.append(math.floor(math.log(feature, self.binSize)))
                else:
                    binnedVector.append(0)
            graphletVectorsBinned.append(binnedVector)

        return graphletVectorsBinned

    ##Calculate Roles based on Node degree
    def calculateRolesDegree(self):
        G = self.G
        nodes = G.nodes

        roleDict = dict()
        for node in nodes:
            roleDict[node] = G.degree(node)

        return roleDict

    # Returns the list of roles visited in a random walk starting from startNode
    def randomWalk(self, startNode, numSteps) -> list:
        G = self.G
        role = self.roleDict
        walk = []
        lastNode = startNode

        for i in range(numSteps):
            walk.append(role[lastNode])
            if len(list(G.neighbors(lastNode))) == 0:
                return walk
            lastNode = random.choice(list(G.neighbors(lastNode)))

        return walk

    ##Returns a number of random walks from each node. numWalks specifies the number of walks carried out from each node, numSteps the steps in each walk
    def generateRandomWalks(self, numWalks, numSteps):
        G = self.G
        walks = []

        for v in G.nodes:
            for i in range(numWalks):
                walks.append(self.randomWalk(v, numSteps))

        return walks

    ##Executes the training of a skip-gram model from Word2Vec
    def skipGram(self, dimension, window) -> Word2Vec:
        return Word2Vec(
            self.walks, vector_size=dimension, window=window, sg=1, workers=8
        )

    ##Generate an embedding for each node according to its node
    def nodeEmbedding(self):
        G = self.G
        roleDict = self.roleDict
        roleEmbedding = self.roleEmbedding

        embedding = dict()
        for n in G.nodes:
            embedding[n] = roleEmbedding.wv[roleDict[n]]

        return embedding
