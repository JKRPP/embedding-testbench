import pathlib
import random
from matplotlib import pyplot as plt
import networkx as nx
import dgl.data
import sklearn.model_selection

from .tuToNx import TUtoNX


class GraphGenerator:
    def __init__(self, set="training"):
        self.graphSet = set

        ##Properties of the DD graph
        self.ddTrainingSet = range(1, 707)
        self.ddValidationSet = range(708, 943)
        self.ddTestSet = range(944, 1178)

        ##Properties of the DHFR graph
        self.dhfrTrainingSet = range(1, 454)
        self.dhfrValidationSet = range(455, 606)
        self.dhfrTestSet = range(607, 756)

        ##Properties of the facebook graph
        self.facebookFeatures = [77, 78]
        self.facebookTrainingSet = [0, 107, 348, 414, 686, 698]
        self.facebookValidationSet = [1684, 1912]
        self.facebookTestSet = [3437, 3980]

    ##Generates a bar bell graph of specified size
    def generateBarbellGraph(self, bellSize, barSize, labelMethod="bells"):
        G = nx.barbell_graph(bellSize, barSize)
        labels = []
        graphSize = len(G.nodes)

        if not labelMethod in ["bells", "type", "combined"]:
            raise Exception("Label method must be bells or type")

        if labelMethod == "bells":
            for i in range(len(G.nodes)):
                if i < graphSize / 2:
                    labels.append(0)
                else:
                    labels.append(1)
        elif labelMethod == "type":
            for i in range(bellSize):
                labels.append(0)
            for i in range(barSize):
                labels.append(1)
            for i in range(bellSize):
                labels.append(0)
        elif labelMethod == "combined":
            for i in range(bellSize):
                labels.append(0)
            for i in range(barSize):
                labels.append(1)
            for i in range(bellSize):
                labels.append(2)

        return (G, labels)

    ##Generates a hollow bar bell graph
    def generateHollowBarbellGraph(
        self, bellSize, barSize, labelMethod="bells", numGraphs=1
    ):
        G = nx.Graph()
        labels = []
        graphSize = bellSize * 2 + barSize
        bell = nx.cycle_graph(bellSize)
        bar = nx.path_graph(barSize)
        G = nx.disjoint_union(bell, bar)
        G = nx.disjoint_union(G, bell)
        G.add_edge(bellSize - 1, bellSize)
        G.add_edge(bellSize - 1 + barSize, bellSize + barSize)

        if not labelMethod in ["bells", "type", "combined"]:
            raise Exception("Label method must be bells or type")

        if labelMethod == "bells":
            for i in range(len(G.nodes)):
                if i < graphSize / 2:
                    labels.append(0)
                else:
                    labels.append(1)
        elif labelMethod == "type":
            for i in range(bellSize):
                labels.append(0)
            for i in range(barSize):
                labels.append(1)
            for i in range(bellSize):
                labels.append(0)
        elif labelMethod == "combined":
            for i in range(bellSize):
                labels.append(0)
            for i in range(barSize):
                labels.append(1)
            for i in range(bellSize):
                labels.append(2)

        return (G, labels)

    ##Generates a caveman graph (a graph of cliquecount isolated cliques of size cliquesize)
    def generateCavemanGraph(self, cliqueCount, cliqueSize):
        G = nx.caveman_graph(cliqueCount, cliqueSize)
        labels = []

        for i in range(cliqueCount):
            for o in range(cliqueSize):
                labels.append(i)

        return (G, labels)

    ##Generates Graphs from the DGL library
    def generateDGLGraph(self, type="karate"):
        if type == "karate":
            dataset = dgl.data.KarateClubDataset()
        if type == "Cora":
            dataset = dgl.data.CoraGraphDataset()
        if type == "texas":
            dataset = dgl.data.TexasDataset()
        if type == "chameleon":
            dataset = dgl.data.ChameleonDataset()
        if type == "amazonphoto":
            dataset = dgl.data.AmazonCoBuyPhotoDataset()
        Gmult = dgl.to_networkx(dataset[0])
        G = nx.DiGraph(Gmult)

        labels = list(dataset[0].ndata["label"])

        return (G, labels)

    ##Generates a graph with circleCount circles of random sizes in the given range
    def generateRandomCircleGraph(self, circleCount, circleSizeRange=(5, 10)):
        G = nx.Graph()
        labels = []

        for i in range(circleCount):
            circleSize = random.randint(circleSizeRange[0], circleSizeRange[1])
            C = nx.cycle_graph(circleSize)
            G = nx.disjoint_union(G, C)
            for o in range(circleSize):
                labels.append(i)

        return (G, labels)

    ##Generates a graph consisting of starCount disconnected stars of size within starsizeRange
    def generateRandomStarsGraph(
        self, starCount, starSizeRange=(5, 10), labelMethod="center"
    ):
        G = nx.Graph()
        labels = []

        for i in range(starCount):
            starSize = random.randint(starSizeRange[0], starSizeRange[1])
            S = nx.star_graph(starSize)
            G = nx.disjoint_union(G, S)
            if labelMethod == "center":
                labels.append(0)
                for o in range(starSize):
                    labels.append(1)

        return (G, labels)

    ##Generates a graph from the facebook dataset
    def generateFacebookGraph(self):
        ##Select a graph from the current set
        if self.graphSet == "training":
            graphID = random.choice(self.facebookTrainingSet)
        if self.graphSet == "validation":
            graphID = random.choice(self.facebookValidationSet)
        if self.graphSet == "test":
            graphID = random.choice(self.facebookTestSet)

        graphID = 0

        ##Read the features from the feature list
        featureLines = (
            open(
                str(pathlib.Path().resolve().as_posix())
                + "code/realWorldGraphs/facebook/"
                + str(graphID)
                + ".feat",
                "r",
            )
            .read()
            .splitlines()
        )
        ##Read features for node 0
        egoFeatures = (
            open(
                str(pathlib.Path().resolve().as_posix())
                + "code/realWorldGraphs/facebook/"
                + str(graphID)
                + ".egofeat",
                "r",
            )
            .read()
            .splitlines()
        )
        ##Read the edges for the graph
        edgeList = (
            open(
                str(pathlib.Path().resolve().as_posix())
                + "code/realWorldGraphs/facebook/"
                + str(graphID)
                + ".edges",
                "r",
            )
            .read()
            .splitlines()
        )
        ##Add all nodes from the featurelist
        G = nx.Graph()
        renameDict = dict()

        allFeatures = ["0 " + egoFeatures[0]] + featureLines
        labels = []

        for index, nodeFeatures in enumerate(allFeatures):
            intFeatures = [int(var) for var in nodeFeatures.split(" ")]
            G.add_node(index)
            renameDict[intFeatures[0]] = index
            if not index == 0:
                G.add_edge(0, index)

            labelAssigned = False
            for index, feature in enumerate(self.facebookFeatures):
                if intFeatures[feature + 1] == 1:
                    labels.append(index + 1)
                    labelAssigned = True
                    break
            if not labelAssigned:
                labels.append(0)

        ##Add all edges from the edgelist, renaming the edges according to the dict
        for edge in edgeList:
            edgeInts = [int(nodeId) for nodeId in edge.split(" ")]
            G.add_edge(renameDict[edgeInts[0]], renameDict[edgeInts[1]])

        return (G, labels)

    ##Loads a random graph from the DHFR dataset
    def generateDHFRGraph(self):
        ##Select a graph from the current set
        if self.graphSet == "training":
            graphID = random.choice(self.dhfrTrainingSet)
        if self.graphSet == "validation":
            graphID = random.choice(self.dhfrValidationSet)
        if self.graphSet == "test":
            graphID = random.choice(self.dhfrTestSet)

        (G, labels) = TUtoNX(
            str(pathlib.Path().resolve().as_posix())
            + "/code/embeddingtester/realWorldGraphs/DHFR/DHFR_A.txt",
            str(pathlib.Path().resolve().as_posix())
            + "/code/embeddingtester/realWorldGraphs/DHFR/DHFR_graph_indicator.txt",
            str(pathlib.Path().resolve().as_posix())
            + "/code/embeddingtester/realWorldGraphs/DHFR/DHFR_node_labels.txt",
        ).readSingleGraph(graphID)

        return (G, labels)

    ##Loads a random graph from the DD dataset
    def generateDDGraph(self):
        ##Select a graph from the current set
        if self.graphSet == "training":
            graphID = random.choice(self.ddTrainingSet)
        if self.graphSet == "validation":
            graphID = random.choice(self.ddValidationSet)
        if self.graphSet == "test":
            graphID = random.choice(self.ddTestSet)

        (G, labels) = TUtoNX(
            str(pathlib.Path().resolve().as_posix())
            + "/code/embeddingtester/realWorldGraphs/DD/DD_A.txt",
            str(pathlib.Path().resolve().as_posix())
            + "/code/embeddingtester/realWorldGraphs/DD/DD_graph_indicator.txt",
            str(pathlib.Path().resolve().as_posix())
            + "/code/embeddingtester/realWorldGraphs/DD/DD_node_labels.txt",
        ).readSingleGraph(graphID)

        return (G, labels)

    ##Generates a graph with groupCount groups of groupSize nodes that are connected to to nodes of the same group with likelihood pIn and nodes of other groups with likelihood pOut
    def generatePlantedPartitionGraph(self, groupCount, groupSize, pIn, pOut):
        G = nx.planted_partition_graph(
            l=groupCount,
            k=groupSize,
            p_in=pIn,
            p_out=pOut,
        )
        labels = []
        for i in range(groupCount):
            for o in range(groupSize):
                labels.append(i)

        return (G, labels)

    def generateNonConnectedGraph(self, nodeCount):
        G = nx.Graph()
        labels = []
        for i in range(nodeCount):
            G.add_node(i)
            labels.append(i)

        return (G, labels)

    def generateRandomGraph(self, nodeCount, probability, labelMethod="degree"):
        G = nx.gnp_random_graph(nodeCount, probability)
        labels = []
        if labelMethod == "degree":
            for n in G.nodes():
                labels.append(G.degree(n))

        print(labels)
        return (G, labels)

    ##Adds noise to the graph by creating a random graph with edge probability noiseProb and xor-ing the edges of G
    def addNoiseToGraph(self, G: nx.Graph, noiseProb) -> nx.Graph():
        if noiseProb == 0:
            return (G, 0)

        noiseGraph = nx.gnp_random_graph(
            G.number_of_nodes(), noiseProb, directed=G.is_directed
        )
        Gnoisy = G

        noiseEdgeNum = noiseGraph.number_of_edges()

        for u, v in noiseGraph.edges:
            if G.has_edge(u, v):
                Gnoisy.remove_edge(u, v)
                # print("Removed edge: " + str((u,v)))
            else:
                Gnoisy.add_edge(u, v)
                # print("Added edge: " + str((u,v)))

        return (Gnoisy, noiseEdgeNum)

    ##Divides the input graph into training, validation and test sets
    def createSplits(self, G: nx.Graph):
        nodes = list(G.nodes())
        testSplit = sklearn.model_selection.train_test_split(
            nodes, train_size=0.8, test_size=0.2
        )
        testSet = testSplit[1]
        validationSplit = sklearn.model_selection.train_test_split(
            testSplit[0], train_size=0.75, test_size=0.25
        )
        trainSet = validationSplit[0]
        validationSet = validationSplit[1]

        return (sorted(trainSet), sorted(validationSet), sorted(testSet))
