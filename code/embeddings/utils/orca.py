import networkx as nx
import orcastr


##Count the Graphlet isomorophic classes a given node participates in
def orbit_counts(task, size, graph):
    undirGraph = nx.Graph(graph.to_undirected())

    undirGraph = nx.convert_node_labels_to_integers(undirGraph)

    undirGraph.remove_edges_from(nx.selfloop_edges(undirGraph))

    s = "{} {}\n".format(len(undirGraph), len(undirGraph.edges))
    for u, v in undirGraph.edges:
        s += "{} {}\n".format(u, v)
    out = orcastr.motif_counts_str(task, size, s)
    out = [[int(c) for c in s.split(" ")] for s in out.split("\n") if s]
    return out
