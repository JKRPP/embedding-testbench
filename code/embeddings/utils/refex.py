from math import ceil
import networkx as nx


class ReFeX:
    def __init__(self, G: nx.Graph, threshold=1, delta=0.1, recursionSteps=3):
        self.G = G
        self.featureVectors = []
        self.delta = delta
        self.th = threshold
        self.featuresAdded = True
        neighborhoodFeatureVectors = []
        for node in G.nodes():
            nodeFeatures = []
            nodeFeatures.extend(self.createLocalFeatures(node))
            nodeFeatures.extend(self.createEgonetFeatures(node))
            neighborhoodFeatureVectors.append(nodeFeatures)

        self.featureVectors = self.vecticalBinVectors(neighborhoodFeatureVectors)
        for i in range(recursionSteps):
            newFeatures = self.recursionStep(self.featureVectors)
            newFeaturesPruned = self.pruneResults(self.featureVectors, newFeatures)
            if not self.featuresAdded:
                break
            for vector in range(len(self.featureVectors)):
                self.featureVectors[vector].extend(newFeaturesPruned[vector])

    def createLocalFeatures(self, node):
        G = self.G
        featVector = [G.degree(node)]
        if G.is_directed():
            featVector.extend([G.in_degree(node), G.out_degree(node)])

        return featVector

    def createEgonetFeatures(self, node):
        G = self.G
        egoNet = nx.ego_graph(G, node)
        featVector = [egoNet.number_of_edges()]

        if G.is_directed():
            outEdges = 0
            inEdges = 0

            for egoNode in egoNet.nodes():
                outEdges += G.out_degree(egoNode) - egoNet.out_degree(egoNode)
                inEdges += G.in_degree(egoNode) - egoNet.in_degree(egoNode)

            featVector.extend([inEdges, outEdges])

        else:
            outEdges = 0

            for egoNode in egoNet.nodes():
                outEdges += G.degree(egoNode) - egoNet.degree(egoNode)
            featVector.append(outEdges)

        return featVector

    def createRecursiveFeaturesNode(self, node, featureVectors):
        neighbors = list(nx.neighbors(self.G, node))
        sumVector = []
        meanVector = []
        for i in range(len(featureVectors[0])):
            sum = 0
            for neighbor in neighbors:
                sum += featureVectors[neighbor][i]

            sumVector.append(sum)
            degree = max(self.G.degree(node), 1)
            meanVector.append(sum / degree)

        recursiveFeatures = []
        recursiveFeatures.extend(sumVector)
        recursiveFeatures.extend(meanVector)

        return recursiveFeatures

    def recursionStep(self, featureVectors):
        G = self.G
        newFeatures = []

        for node in G.nodes():
            newFeatures.append(self.createRecursiveFeaturesNode(node, featureVectors))

        newFeaturesBinned = self.vecticalBinVectors(newFeatures)
        return newFeaturesBinned

    ##Bin vectors
    def vecticalBinVectors(self, featureVectors):
        delta = self.delta

        numVectors = len(featureVectors)
        numFeatures = len(featureVectors[0])

        ##calculate the log binning bin borders
        binBorders = []
        border = 0
        remainingLen = numVectors

        while remainingLen > 0:
            binsize = ceil(remainingLen * delta)
            border += binsize
            remainingLen -= binsize
            binBorders.append(border)

        ##log-bin the counts
        # featureVectorsBinned = copy.deepcopy(featureVectors)

        featureBins = []

        for featureIndex in range(numFeatures):
            featureValues = []
            featureValueBorders = []
            for featureVector in featureVectors:
                featureValues.append(featureVector[featureIndex])

            featureValuesSorted = sorted(featureValues)
            for border in binBorders:
                featureValueBorders.append(featureValuesSorted[border - 1])
            featureBins.append(featureValueBorders)

        featureVectorsBinned = []

        for featureVector in featureVectors:
            binnedVector = []
            for i in range(numFeatures):
                for index, bin in enumerate(featureBins[i]):
                    if featureVector[i] <= bin:
                        binnedVector.append(float(index))
                        break
                    elif index == len(featureBins[i]) - 1:
                        binnedVector.append(float(index))
            featureVectorsBinned.append(binnedVector)

        return featureVectorsBinned

    def pruneResults(self, oldFeatures, newFeatures):
        newFeatCount = len(newFeatures[0])
        oldFeatCount = len(oldFeatures[0])

        vectorCount = len(newFeatures)

        correlatedFeatures = []

        for i in range(newFeatCount):
            maxDiversion = 0
            for o in range(oldFeatCount):
                for vector in range(vectorCount):
                    maxDiversion = max(
                        [
                            maxDiversion,
                            abs(newFeatures[vector][i] - oldFeatures[vector][o]),
                        ]
                    )
            if maxDiversion <= self.th:
                correlatedFeatures.append(i)

        prunedNewFeatures = []

        for vector in range(vectorCount):
            prunedNewFeatVector = []
            for i in range(newFeatCount):
                if i in correlatedFeatures:
                    continue
                prunedNewFeatVector.append(newFeatures[vector][i])
            if len(prunedNewFeatVector) == 0:
                self.featuresAdded = False
                break
            prunedNewFeatures.append(prunedNewFeatVector)

        return prunedNewFeatures


G = nx.gnm_random_graph(300, 500)
rfx = ReFeX(G)
