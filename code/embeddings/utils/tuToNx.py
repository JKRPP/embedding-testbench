import random
import networkx as nx
import matplotlib.pyplot as plt


class TUtoNX:
    def __init__(self, edgePath, indicatorPath, labelPath):
        self.edgePath = edgePath
        self.indicatorPath = indicatorPath
        self.labelPath = labelPath

    def readGraphOfSize(self, graphSize=100, offset=0):
        edgeFile = open(
            "code/realworldgraphs/deezer_ego_nets/deezer_ego_nets_A.txt",
            "r",
        )
        edges = [edgeLine.split(", ") for edgeLine in edgeFile.read().splitlines()]
        edgeSubSet = edges[offset : offset + graphSize]
        G = nx.Graph()
        G.add_edges_from(edgeSubSet)
        return G

    def readSingleGraph(self, graphIndex):
        indicatorFile = open(
            self.indicatorPath,
            "r",
        )
        lines = [int(i) for i in indicatorFile.read().splitlines()]
        startNode = lines.index(graphIndex)
        endNode = lines.index(graphIndex + 1) + 1

        nodeList = range(endNode - (startNode + 1))
        labelList = []

        labelFile = open(self.labelPath, "r").read().splitlines()
        for node in nodeList:
            labelList.append(int(labelFile[node + startNode]))

        edgeFile = open(
            self.edgePath,
            "r",
        )
        edges = [
            list(map(int, edgeLine.split(", ")))
            for edgeLine in edgeFile.read().splitlines()
        ]

        startEdgeIndex = 0
        endEdgeIndex = 0

        for indx, edgeLine in enumerate(edges):
            if startNode in edgeLine:
                startEdgeIndex = indx + 1
            if endNode in edgeLine:
                endEdgeIndex = indx
                break

        fileEdges = edges[startEdgeIndex : endEdgeIndex - 1]

        G = nx.Graph()
        G.add_nodes_from(nodeList)

        for edge in fileEdges:
            G.add_edge(edge[0] - (startNode + 1), edge[1] - (startNode + 1))

        return (G, labelList)
