import os
import pathlib
import time
import csv
import numpy as np
from embeddings.embeddingTestBench import EmbeddingTestBench


##Runs the experiment specified in the experiment file
class TestBenchTrialer:
    def __init__(self):
        numRunsPerStep = 1
        numSteps = 1

        self.graphType = "random"

        self.escalateNoise = False
        self.startingNoise = 0.00
        self.noiseProgression = 0.02

        self.escalatePOut = False
        self.startingPout = 0
        self.pOutProgression = 0.02

        self.escalateDimension = False
        self.startingdimension = 16
        self.escalateDimensionExponential = True
        self.dimensionProgression = 128

        self.trialTitle = str(input("Please enter a name for trial: "))

        self.trialFolder = (
            str(pathlib.Path().resolve().as_posix())
            + "/results/"
            + str(int(time.time()))
            + "_"
            + self.trialTitle
            + "/"
        )

        os.mkdir(self.trialFolder)

        self.currNoise = self.startingNoise
        self.currPOut = self.startingPout
        self.currDimension = self.startingdimension

        for i in range(numSteps):
            self.trialResults = []

            for o in range(numRunsPerStep):
                ##Run all embeddings specified in embeddingtestbench
                if i == 0 and o == 0:
                    self.computeRun(
                        0, 0, self.currPOut, self.currNoise, self.currDimension
                    )
                    print("First run done")
                    continue
                self.computeRun(i, o, self.currPOut, self.currNoise, self.currDimension)

            avgResults = list(np.mean(np.array(self.trialResults), axis=0))
            avgRow = [str(i), str(self.currNoise)]
            avgRow.extend(avgResults)
            averageFile = open(self.trialFolder + "/averages.csv", "a", newline="")
            avgWriter = csv.writer(averageFile)
            avgWriter.writerow(avgRow)
            averageFile.close()

            if self.escalateNoise:
                self.currNoise += self.noiseProgression
            if self.escalatePOut:
                self.currPOut += self.pOutProgression
            if self.escalateDimension:
                if self.escalateDimensionExponential:
                    self.currDimension = self.currDimension * 2
                else:
                    self.currDimension += self.dimensionProgression

        self.resultFile.close()

    def computeRun(self, trial, run, pOut=0.3, noise=0.0, dimension=8):
        etb = EmbeddingTestBench(
            noiseProb=noise,
            pOut=pOut,
            dimension=dimension,
            trialFolder=self.trialFolder,
            folderSuffix=(self.trialTitle + "_" + str(trial) + "_" + str(run)),
            graphtype=self.graphType,
        )

        ##On first run, initialise raw result and average file
        if run == 0 and trial == 0:
            firstRow = ["run", "trial"]
            if self.escalateNoise:
                firstRow.append("noise")
            if self.escalatePOut:
                firstRow.append("pOut")
            if self.escalateDimension:
                firstRow.append("dimension")
            firstRow.extend(etb.embeddingNames)

            self.resultFile = open(self.trialFolder + "/results.csv", "a", newline="")
            self.resultWriter = csv.writer(self.resultFile)
            self.resultWriter.writerow(firstRow)
            self.resultFile.close()

            del firstRow[0]

            self.averageFile = open(self.trialFolder + "/averages.csv", "w", newline="")
            self.avgWriter = csv.writer(self.averageFile)
            self.avgWriter.writerow(firstRow)
            self.averageFile.close()

        ##Write result to raw results file every run
        self.trialResults.append([])

        runRow = [str(run), str(trial)]
        if self.escalateNoise:
            runRow.append(str(self.currNoise))
        if self.escalatePOut:
            runRow.append(str(self.currPOut))
        if self.escalateDimension:
            runRow.append(str(self.currDimension))

        for embeddingScores in etb.embeddingMLPScoresByEmbedding:
            runRow.append(np.mean(embeddingScores))
            self.trialResults[run].append(np.mean(embeddingScores))

        resultFile = open(self.trialFolder + "/results.csv", "a", newline="")
        resultWriter = csv.writer(resultFile)
        resultWriter.writerow(runRow)
        resultFile.close()


if __name__ == "__main__":
    tbt = TestBenchTrialer()
